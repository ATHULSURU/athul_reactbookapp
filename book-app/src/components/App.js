import React from "react";

import book from "../model/book";
import MyBook from "./MyBook";
import NewBook from "./AddBook";

class App extends React.Component {
  
  
  constructor() {
    super()
    this.state = {
       listBooks:[],
        books: book,
        showForm: false,    
    }
    this.deleteUser=this.deleteUser.bind(this)
   
}

deleteUser =(index) =>{ 
  const books=Object.assign([],this.state.books);
  books.splice(index,1);
  this.setState({books:books})
}

  addNewBookForm = () => {
    this.setState(prevState => {
      return {
        showForm: prevState.showForm!==true
      };
    });
  };

  addBook = (book) => {
    book.id =
      Math.max.apply(null, this.state.books.map(book => book.id)) + 1;
    if (book.title && book.details) {
      this.setState(prevState => {
        return {
          books: [book, ...prevState.books]
        };
      });
    }
  };


render() {
  const listBooks = this.state.books.map((myBook) => (
    <MyBook key={myBook.id}
     delElemnt={this.deleteUser}
     mybooks={myBook} />
  ));
  const newBook = this.state.showForm ? (
    <NewBook add={this.addBook} />
  ) : (
      <div onClick={this.addNewBookForm}>
        Add Book
    </div>
    );


  return( 
      <div>
        <div className="main-content">
          {newBook}
          <div className="wrapper">{listBooks}</div>
        </div>
      </div>
    );
  }}

export default App;
