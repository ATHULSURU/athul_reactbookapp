import React from "react";

class NewBook extends React.Component {
  constructor() {
    super();
    this.state = {
      title: "",
      details: ""
    };
    this.handleSubmit=this.handleSubmit.bind(this)
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  }
  handleSubmit = (e) => {
    e.preventDefault()
    this.props.add(this.state)
  }

  render() {
    return (
      <div >
        <h2>Add Book</h2>
        <form onSubmit={this.handleSubmit}>
          <input type="text" name="title" value={this.state.title} placeholder="Book Title" onChange={this.handleChange}
          />
          <br />
          <textarea type="text" name="details" value={this.state.details} placeholder="Book Details" onChange={this.handleChange}
          />
          <br />
          <button>Submit</button>
        </form>
      </div>
    );
    }
}

export default NewBook;
