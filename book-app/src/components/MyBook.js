import React from "react";

function MyBook(props) {
  // console.log(props);

  return (
    <div>
      <h2>Id:{props.mybooks.id}</h2>
      <h3>Title: {props.mybooks.title}</h3>
      <p>Details: {props.mybooks.details}</p>
      <button onClick={props.delElemnt}>Delete</button>
      <button onClick={() => this.props.edit}>Edit</button>
      <hr />
    </div>
  );
}

export default MyBook;
