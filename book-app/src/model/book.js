const book = [
  {
    id: 1,
    title: "One nyt at call center",
    details:
      "One Night @ the Call Center is a novel written by Chetan Bhagat and first published in 2005."
  },
  {
    id: 2,
    title: "Rich dad poor dad",
    details:
      "Rich Dad Poor Dad is a 1997 book written by Robert Kiyosaki and Sharon Lechter."
  },
  {
    id: 3,
    title: "Business of 21st century",
    details:
      "In The Business of the 21st Century, Robert Kiyosaki explains the revolutionary business of network marketing"
  }
];

export default book;
